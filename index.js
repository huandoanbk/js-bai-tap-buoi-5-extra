//Function chung

function getID(IDname) {
  return document.getElementById(IDname);
}
/**
 * Bài 1
 */
var hoTen, thuNhap, phuThuoc, tienThue;
var muc1 = 0.05; //60tr
var muc2 = 0.1; //+60tr
var muc3 = 0.15; //+90tr
var muc4 = 0.2; //+174tr
var muc5 = 0.25; //+240tr
var muc6 = 0.3; //+336tr
var muc7 = 0.35;

function tinhThue() {
  hoTen = getID("hoTen").value;
  thuNhap = getID("thuNhap").value * 1;
  phuThuoc = getID("soPhuthuoc").value * 1;
  tienThue = 0;
  var chiuThue = thuNhap - 4e6 - phuThuoc * 1600000;
  if (thuNhap < 0 || phuThuoc < 0) {
    alert("Số liệu không hợp lệ! Vui lòng kiểm tra lại.");
    var soThue = getID("tienThue");
    soThue.innerText = "";
  } else if (chiuThue <= 0) {
    tienThue = 0;
  } else if (chiuThue > 0 && chiuThue <= 60e6) {
    tienThue = chiuThue * muc1;
  } else if (chiuThue > 60e6 && chiuThue <= 120e6) {
    tienThue = 60e6 * muc1 + (chiuThue - 60e6) * muc2;
  } else if (chiuThue > 120e6 && chiuThue <= 210e6) {
    tienThue = 60e6 * muc1 + 60e6 * muc2 + (chiuThue - 120e6) * muc3;
  } else if (chiuThue > 210e6 && chiuThue <= 384e6) {
    tienThue =
      60e6 * muc1 + 60e6 * muc2 + 90e6 * muc3 + (chiuThue - 210e6) * muc4;
  } else if (chiuThue > 384e6 && chiuThue <= 624e6) {
    tienThue =
      60e6 * muc1 +
      60e6 * muc2 +
      90e6 * muc3 +
      174e6 * muc4 +
      (chiuThue - 384e6) * muc5;
  } else if (chiuThue > 624e6 && chiuThue <= 960e6) {
    tienThue =
      60e6 * muc1 +
      60e6 * muc2 +
      90e6 * muc3 +
      174e6 * muc4 +
      240e6 * muc5 +
      (chiuThue - 624e6) * muc6;
  } else if (chiuThue > 960e6) {
    tienThue =
      60e6 * muc1 +
      60e6 * muc2 +
      90e6 * muc3 +
      174e6 * muc4 +
      240e6 * muc5 +
      336e6 * muc6 +
      (chiuThue - 960e6) * muc7;
  }
  var soThue = getID("tienThue");
  soThue.innerText = `Họ tên: ${hoTen}. Tiền thuế: ${new Intl.NumberFormat().format(
    tienThue
  )} VNĐ`;
}
/**
 * Bài 2
 */
var loaiNguoidan = true;
var loaiDnghiep = false;
function loaiThuebao() {
  loaiNguoidan = getID("nhaDan").checked;
  loaiDnghiep = getID("doanhNghiep").checked;
  if (loaiDnghiep) {
    getID("soKN").classList.remove("d-none");
  }
  if (loaiNguoidan) {
    getID("soKN").classList.add("d-none");
  }
}

var phiXLnhaDan = 4.5;
var phiCobannhaDan = 20.5;
var phiCaocapnhaDan = 7.5;
var phiXLDnghiep = 15;
var phiCobanDnghiep = 75;
var phiCobanDnghiepXtra = 5;
var phiCaocapDnghiep = 50;
var maKH, soKetnoi, kenhCaocap, tienCap;
function tinhTiencap() {
  maKH = getID("maKH").value;
  soKetnoi = getID("soKetnoi").value * 1;
  kenhCaocap = getID("kenhCaocap").value * 1;
  tienCap = 0;
  if ((loaiDnghiep && soKetnoi < 0) || kenhCaocap < 0) {
    alert("Dữ liệu không hợp lệ!");
  } else if (loaiNguoidan) {
    tienCap = phiXLnhaDan + phiCobannhaDan + kenhCaocap * phiCaocapnhaDan;
  } else if (loaiDnghiep) {
    tienCap = phiXLDnghiep + phiCobanDnghiep + kenhCaocap * phiCaocapDnghiep;
    if (soKetnoi > 10) {
      tienCap = tienCap + (soKetnoi - 10) * phiCobanDnghiepXtra;
    }
  }
  getID(
    "tienCap"
  ).innerText = `Mã Khách hàng: ${maKH}. Tiền cáp: ${new Intl.NumberFormat().format(
    tienCap
  )} USD`;
}
